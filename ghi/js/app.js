function createCard(name, description, pictureUrl) {
    return `
    <div class="card shadow mb-4">
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="card-text">${description}</p>
        </div>
      </div>
    </div>
    `;
  }

  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            throw new Error("Error getting data from API");
        } else {
            const data = await response.json();

            let counter = 0 // keeps track of which column we want our content in

            for (let i = 0; i < data.conferences.length; i++) {
                const detailUrl = `http://localhost:8000${data.conferences[i].href}`;
                const detailResponse = await fetch(detailUrl);

                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const html = createCard(title, description, pictureUrl);
                    const columns = document.querySelectorAll('.col'); // columns is a NodeList of all locations where 'col' appears
                    const column = columns[counter] // column is an object; grabs the location from the querySelector (div col 0, div col 1, div col 2)
                    column.innerHTML += html; // put the card information in html into that specific column
                    counter++
                    if (counter === 3) {
                        counter = 0
                    }
                }
            }
        }
    } catch (e) {
        // Figure out what to do if an error is raised
        console.error(e);
    }

});



// window.addEventListener('DOMContentLoaded', async () => {

//     const url = "http://localhost:8000/api/conferences/";
//    try{
//     const response = await fetch(url);
//     // console.log(response);
//     if (!response.ok){
//         // figure out what to do when the response is bad
//         // throw new Error("Error getting data from API");
//     } else {
//         const data = await response.json();
//         // Display conference name
//         const conference = data.conferences[0];
//         const nameTag = document.querySelector(".card-title");
//         nameTag.innerHTML = conference.name;

//         const detailURL = `http://localhost:8000${conference.href}`;
//         const detailResponse = await fetch(detailURL);
//         if (detailResponse.ok) {
//             const details = await detailResponse.json();
//            console.log(details);

//         // Get conference description
//             const conferenceDetail = details.conference.description
//             const descriptionTag = document.querySelector(".card-text");
//             descriptionTag.innerHTML = conferenceDetail;
//             const imageTag = document.querySelector(".card-img-top");
//             imageTag.src = details.conference.location.picture_url;
//         }
//         // console.log(conference);
//         }

//     } catch (e) {
//         // figure out what to do if error is raised
//         // console.error(e)
//     }

// });
